package si.uni_lj.fri.pbd.lab5;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class TimerService extends Service {

    private static final String TAG = TimerService.class.getSimpleName();

    public static final String ACTION_STOP = "stop_service";

    public static final String ACTION_START = "start_service";

    private static final String channelID = "background_timer";

    // TODO: define a static final int NOTIFICATION_ID
      static final int NOTIFICATION_ID=6;

    // Start and end times in milliseconds
    private static long startTime, endTime;

    // Is the service tracking time?
    private static boolean isTimerRunning;

    // TODO: Define serviceBinder and instantiate it to RunServiceBinder
    private final IBinder serviceBinder=new RunServiceBinder();

    @Override
    public void onCreate() {
        Log.d(TAG, "Creating service");

        // TODO: set startTime, endTime, isTimerRunning to default values
         startTime=0;
         endTime=0;
         isTimerRunning=false;
        // TODO: create notification channel
        createNotificationChannel();
    }


    /**
     * Starts the timer
     */
    public static void startTimer() {
        if (!isTimerRunning) {
            startTime = System.currentTimeMillis();
            isTimerRunning = true;
        }
        else {
            Log.e(TAG, "startTimer request for an already running timer");
        }
    }

    /**
     * Stops the timer
     */
    public static void stopTimer() {
        if (isTimerRunning) {
            endTime = System.currentTimeMillis();
            isTimerRunning = false;
        }
        else {
            Log.e(TAG, "stopTimer request for a timer that isn't running");
        }
    }


    /**
     * @return whether the timer is running
     */
    public static boolean isTimerRunning() {
        return isTimerRunning;
    }


    /**
     * Returns the  elapsed time
     *
     * @return the elapsed time in seconds
     */
    public static long elapsedTime() {
        // If the timer is running, the end time will be zero
        return endTime > startTime ?
                (endTime - startTime) / 1000 :
                (System.currentTimeMillis() - startTime) / 1000;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting service");

        // TODO: check the intent action and if equal to ACTION_STOP, stop the foreground service
        if(intent.getAction()==ACTION_STOP){
            stopForeground(true);
            stopSelf();
        }
        return Service.START_STICKY;
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");

        // TODO: Return serviceBinder
        return this.serviceBinder;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destroying service");
    }


    /**
     * Creates a notification for placing the service into the foreground
     *
     * @return a notification for interacting with the service when in the foreground
     */
    // TODO: Uncomment for creating a notification for the foreground service
     private Notification createNotification() {

        // TODO: add code to define a notification action
         Intent actionIntent = new Intent(this, TimerService.class);
         actionIntent.setAction(ACTION_STOP);
         PendingIntent actionPendingIntent = PendingIntent.getService(this, 0,
                 actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelID)
                .setContentTitle(getString(R.string.notif_title))
                .setContentText(getString(R.string.notif_text))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)
                .addAction(android.R.drawable.ic_media_pause, "Stop",
                        actionPendingIntent);


        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0, resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        return builder.build();
    }

    // TODO: Uncomment for creating a notification channel for the foreground service
     private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT < 26) {
            return;
        } else {

            NotificationChannel channel = new NotificationChannel(TimerService.channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(getString(R.string.channel_desc));
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);

            NotificationManager managerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            managerCompat.createNotificationChannel(channel);
        }
    }

    public  void foreground(){
        startForeground(NOTIFICATION_ID,
                createNotification());
    }

    public  void background(){
        stopForeground(true);
    }

    public class RunServiceBinder extends Binder {
        TimerService getService() {
            return TimerService.this;
        }
    }
}



